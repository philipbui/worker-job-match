'use strict';

const axios = require('axios');
const { distance } = require('../../lib/geo-location');

exports.handler = {
	method: 'GET',
	path: '/worker/{id}/jobs/match',
	handler: function (request) {
		const id = parseInt(request.params.id);
		if (isNaN(id)) {
			return 'The endpoint only supports integer Ids';
		}
		
		if (!jobs && !workers) {
			return axios.all([
				axios.get('http://test.swipejobs.com/api/workers'),
				axios.get('http://test.swipejobs.com/api/jobs')
			]).then(axios.spread((workersResp, jobsResp) => {
				workers = workersResp.data;
				jobs = jobsResp.data;					
				return matchWorkerWithJobs(workers, jobs, id);
			})).catch(error => {
				console.error('Found error in Promise Chain', error);
				return error;
			});
		} else {
			return matchWorkerWithJobs(workers, jobs, id)
		}
	}
};

let jobs = null; // Cache to speeden up future requests
let workers = null;

const matchWorkerWithJobs = (workers, jobs, id) => {
	const worker = workers.find(worker => worker.userId == id);
	if (!worker) {
		return `The worker ${id} was not found`;
	}
	
	const scores = [];
	jobs.forEach(job => {
		let score = 0;
		if (!filterRequiredCertificates(worker, job)) {
			console.debug('Filtering due to missing required certificates');
			return;
		}
		
		if (!filterDriverLicenseRequired(worker, job)) {
			console.debug('Filtering due to not meeting drive license requirements');
			return;
		}
		
		const locationScore = filterAndScoreLocation(worker, job);
		if (!locationScore) {
			console.debug('Filtering due to exceeding max location distance');
			return;
		}
		
		const {billRatings, maxWorkers} = getBillRatingsAndMaxWorkers(jobs);
		score += scoreDriverLicenseRequired(worker, job);
		score += locationScore;
		score += scoreBillRatings(worker, job, billRatings);
		score += scoreWorkersRequired(job, maxWorkers);
		score += scoreJobTitle(worker, job);
		scores.push({
			score,
			job
		});
	});
	return scores.sort((a, b) => b.score - a.score).slice(0, 3);
};
exports.matchWorkerWithJobs = matchWorkerWithJobs;

/**
 * Filter and score locations so jobs are within the search distance and closer ones are ranked higher.
 * @param worker A Javascript object representing a Worker
 * @param job A Javascript object representing a Job
 * @returns {number} null for no match. 5-0 depending on the worker to job proximity.
 */
const filterAndScoreLocation = (worker, job) => {
	const {unit, maxJobDistance, latitude, longitude} = worker.jobSearchAddress;
	if (unit !== 'km') {
		console.error('Found invalid distance unit, expected km, found ' + unit);
		return;
	}
	
	const location = job.location;
	const dist = distance(latitude, longitude, location.latitude, location.longitude);
	if (dist > maxJobDistance) {
		return;
	}
	
	for (let i = 1; i < 6; i++) {
		if (dist < maxJobDistance * (i * 0.2)) {
			return 6 - i;
		}
	}
	return 0;
};

/**
 * Filter jobs that require a Driver License and the Worker does not.
 * @param worker A Javascript object representing a Worker
 * @param job A Javascript object representing a Job
 * @returns {boolean} False if the worker does not meet the requirements of the job.
 */
const filterDriverLicenseRequired = (worker, job) => {
	return !job.driverLicenseRequired || (job.driverLicenseRequired && worker.hasDriversLicense);
};

/**
 * Add marginal score to public transport workers with jobs that don't require Driver Licenses
 * @param worker
 * @param job
 * @returns {number}
 */
const scoreDriverLicenseRequired = (worker, job) => {
	return job.transportation == 'PUBLIC TRANSPORT' && !job.driverLicenseRequired ? 1 : 0;
};

const filterRequiredCertificates = (worker, job) => {
	let workerCertificates = new Set(worker.certificates);
	for (let i = 0; i < job.requiredCertificates.length; i++) {
		const requiredCertificate = job.requiredCertificates[i];
		if (!workerCertificates.has(requiredCertificate)) {
			return false;
		}
	}
	return true;
};

const getBillRatingsAndMaxWorkers = (jobs) => {
	let billRatings = [];
	let min = Number.MAX_VALUE, max = 0, maxWorkers = 0;
	jobs.forEach(job => {
		const billRate = parseFloat(job.billRate.replace(/[^0-9.]/g, ''));
		if (min > billRate) {
			min = billRate;
		} else if (max < billRate) {
			max = billRate;
		}
		if (maxWorkers < job.workersRequired) {
			maxWorkers = job.workersRequired;
		}
	});
	
	billRatings.push(min);
	const diff = (max - min) / 4;
	for (let i = 0; i < 3; i++) {
		min += diff;
		billRatings.push(min);
	}
	billRatings.push(max);
	return {
		billRatings,
		maxWorkers
	};
};

const getBillRating = (job, billRatings) => {
	for (let i = 0; i < billRatings.length; i++) {
		if (parseFloat(job.billRate.replace(/[^0-9.]/g, '')) < billRatings[i]) {
			return i + 1;
		}
	}
	return 5;
};

/**
 * Score higher rated users with jobs that have pay bracket matching their skills.
 * @param worker A Javascript object representing a Worker
 * @param job A Javascript object representing a Job
 * @param billRatings
 * @returns {number} Score between 1-5, a higher score meaning the job's pay bracket better represents the worker rating.
 */
const scoreBillRatings = (worker, job, billRatings) => {
	const {rating} = worker;
	const billRating = getBillRating(job, billRatings);
	return 5 - (Math.abs(rating - billRating));
};

/**
 * 1 Worker Jobs should be scored the highest from this endpoint.
 * @param job A Javascript object representing a Job
 * @param maxWorkers Integer of the job with the maximum workers required
 * @returns {number} Score between 0 - maximum worker required. The max score is for jobs with 1 people.
 */
const scoreWorkersRequired = (job, maxWorkers) => {
	return maxWorkers - job.workersRequired;
};

/**
 * Score a worker higher if they have previous experience.
 * @param worker A Javascript object representing a Worker
 * @param job A Javascript object representing a Job
 * @returns {number} 5 if previous experience, else 0.
 */
const scoreJobTitle = (worker, job) => {
	const skills = new Set(worker.skills);
	return skills.has(job.jobTitle) ? 5 : 0;
};
