'use strict';

const matchWorkerIdGet = require('./route/worker/jobs-match').handler;

const Hapi = require('hapi');

const server = new Hapi.Server({port: 3000, host: 'localhost'});

server.start((err) => {
	if (err) {
		throw err;
	}
	
	console.log(`Server running at: ${server.info.uri}`);
});

server.route(matchWorkerIdGet);
