'use strict';

const workerJobsMatch = require('../../../route/worker/jobs-match');
const expect = require('chai').expect;

describe('Worker Jobs Matching', () => {
	describe('Worker Job Matches', () => {
		const worker0 = {
			"rating":2,
			"isActive":true,
			"certificates":[
				"Outstanding Innovator",
				"The Behind the Scenes Wonder",
				"The Risk Taker",
				"Outside the Box Thinker",
				"Marvelous Multitasker",
				"The Asker of Good Questions",
				"Outstanding Memory Award",
				"Office Lunch Expert",
				"Excellence in Organization"
			],
			"skills":[
				"Creator of opportunities",
				"Arts and Crafts Designer"
			],
			"jobSearchAddress":{
				"unit":"km",
				"maxJobDistance":30,
				"longitude":"13.971284",
				"latitude":"49.782281"
			},
			"transportation":"CAR",
			"hasDriversLicense":false,
			"availability":[
				{
					"title":"Tuesday",
					"dayIndex":2
				},
				{
					"title":"Monday",
					"dayIndex":1
				},
				{
					"title":"Sunday",
					"dayIndex":7
				},
				{
					"title":"Thursday",
					"dayIndex":4
				},
				{
					"title":"Friday",
					"dayIndex":5
				},
				{
					"title":"Wednesday",
					"dayIndex":3
				},
				null
			],
			"phone":"+1 (847) 420-3272",
			"email":"fowler.andrews@comcubine.io",
			"name":{
				"last":"Andrews",
				"first":"Fowler"
			},
			"age":30,
			"guid":"562f6647410ecd6bf49146e9",
			"userId":0
		};
		
		const jobNotRequiresDriverLicense = {"driverLicenseRequired":false,"requiredCertificates":[],"location":{"longitude":"13.971284","latitude":"49.782281"},"billRate":"$5.00","workersRequired":1,"startDate":"2015-11-12T09:29:19.188Z","about":"","jobTitle":"","company":"","guid":"562f66aa51a9a4d728a65f6a","jobId":0};
		const jobRequiresDriverLicense = {"driverLicenseRequired":true,"requiredCertificates":[],"location":{"longitude":"13.971284","latitude":"49.782281"},"billRate":"$5.00","workersRequired":1,"startDate":"2015-11-12T09:29:19.188Z","about":"","jobTitle":"","company":"","guid":"562f66aa17b61ab099669893","jobId":1};
		
		it('should fail if no worker was found', () => {
			expect(workerJobsMatch.matchWorkerWithJobs([], [], 0)).to.not.be.a('Array');
			expect(workerJobsMatch.matchWorkerWithJobs([worker0], [], 1)).to.not.be.a('Array');
		});
		
		it('should not fail if no jobs was found', () => {
			testResults(workerJobsMatch.matchWorkerWithJobs([worker0], [], 0));
		});
		
		it('should match a job if it fits the worker', () => {
			const results = workerJobsMatch.matchWorkerWithJobs([worker0], [jobNotRequiresDriverLicense], 0);
			testResults(results);
			expect(results).to.have.length(1);
		});
		
		it('should not match jobs with driver license required if the worker does not have one', () => {
			const jobRequiresDriverLicenseResults = workerJobsMatch.matchWorkerWithJobs([worker0], [jobRequiresDriverLicense], 0);
			testResults(jobRequiresDriverLicenseResults);
			expect(jobRequiresDriverLicenseResults).to.have.length(0);
			
			const jobsWithAndWithoutDriverLicenseResults = workerJobsMatch.matchWorkerWithJobs([worker0], [jobRequiresDriverLicense, jobNotRequiresDriverLicense], 0);
			testResults(jobsWithAndWithoutDriverLicenseResults);
			expect(jobsWithAndWithoutDriverLicenseResults).to.have.length(1);
		});
		
		const testResults = (results) => {
			expect(results).to.be.a('Array');
		}
	})
});